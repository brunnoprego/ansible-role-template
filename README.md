# ansible-role-template

- Repositório modelo de tarefa Ansible para usar como base inicial para outros repositórios
- Ansible rote template repository to use as base to another repositories

## Estrutura de arquivos e diretórios

- ansible-role-template/
  README.md  - este arquivo
  defaults/  - diretório contendo variáveis padrão da tarefa
    .gitkeep - arquivo para manter o diretório no repositório
  files/     - contém arquivos distribuídos com a tarefa
    .gitkeep - arquivo para manter o diretório no repositório
  handlers/  - tratamento de eventos disparados com a tarefa
    .gitkeep - arquivo para manter o diretório no repositório
  meta/      - contém dependências de outras tarefas
    .gitkeep - arquivo para manter o diretório no repositório
  tasks/     - tarefas a serem executadas
    .gitkeep - arquivo para manter o diretório no repositório
  templates/ - diretório de modelos de arquivos da tarefa
    .gitkeep - arquivo para manter o diretório no repositório
  vars/      - variáveis da tarefa
    .gitkeep - arquivo para manter o diretório no repositório
